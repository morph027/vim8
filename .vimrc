let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif

if has("autocmd")
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

set wildmenu
set wildmode=longest:full,full
set wildoptions=pum
set nocompatible
set cursorline
set termguicolors
colorscheme solarized8
set number
set completeopt+=menuone,noselect,noinsert
set backspace=indent,eol,start
syntax on
filetype on
filetype plugin indent on

if executable('starlark-lsp')
    autocmd User lsp_setup call lsp#register_server({
        \ 'name': 'starlark-lsp',
        \ 'cmd': ['starlark-lsp', 'start'],
        \ 'allowlist': ['starlark']
    \ })
endif

if executable('starlark')
    autocmd User lsp_setup call lsp#register_server({
        \ 'name': 'starlark',
        \ 'cmd': ['starlark', '--lsp'],
        \ 'allowlist': ['starlark'],
    \ })
endif

if executable('nc') && executable('pylsp')
    " pip install python-lsp-server python-lsp-ruff ruff
    let pylsp_plugins = {}
    let pylsp_configurationSources = []
    if executable('ruff')
        call extend(pylsp_plugins, {
        \'ruff': {
	\  'enabled': v:true,
	\  'formatEnabled': v:true,
	\  'format': ['I'],
	\  'extendSelect': ['I'],
	\ }
	\})
    endif
    autocmd User lsp_setup call lsp#register_server({
        \'name': 'pylsp',
        \'cmd': ["nc", "localhost", "2087"],
        \'allowlist': ['python'],
        \'workspace_config': {
        \ 'pylsp': {
        \  'configurationSources': pylsp_configurationSources,
        \  'plugins': pylsp_plugins,
        \ },
        \}
    \})
endif

if executable('bash-language-server')
    autocmd User lsp_setup call lsp#register_server({
        \ 'name': 'bash-language-server',
        \ 'cmd': ['bash-language-server', 'start'],
        \ 'allowlist': ['sh'],
    \ })
endif

if executable('yaml-language-server')
    autocmd User lsp_setup call lsp#register_server({
        \ 'name': 'yaml-language-server',
        \ 'cmd': ['yaml-language-server', '--stdio'],
        \ 'allowlist': ['yaml'],
	\ 'workspace_config': {
	\   'yaml': {
	\     'customTags': ['!TAG'],
	\   },
	\ },
    \ })
endif

if executable('gopls')
    autocmd User lsp_setup call lsp#register_server({
        \   'name': 'gopls',
        \   'cmd': ['gopls'],
        \   'allowlist': ['go', 'gomod'],
    \ })
endif

" fold indents on startup
" toggle fold via za
" unfold all via zR
" fold all via zM
command Fold setlocal foldmethod=indent

let g:indent_guides_enable_on_vim_startup = 1

let g:fzf_action = { 'enter': 'tab split' }
nmap <C-f> :FZF<CR>

let g:rg_highlight = 'true'
let g:rg_command = 'rg --vimgrep --hidden'

" no swap files for gopass
autocmd BufNewFile,BufRead /dev/shm/gopass* setlocal noswapfile nobackup noundofile viminfo=""
autocmd BufNewFile,BufRead /private/**/gopass** setlocal noswapfile nobackup noundofile viminfo=""

" Set 'vim-lsp' linter
let g:ale_linters = {
    \ 'python': ['pylsp'],
    \ 'sh': ['bash-language-server', 'shellcheck'],
    \ 'go': ['golint'],
\ }
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_text_changed = 'normal'
let g:ale_list_window_size = 5
let g:ale_close_preview_on_insert = 1
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
nnoremap <F10> :ALEGoToDefinition<CR>
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1

command Jq %!jq '.'

" Put these lines at the very end of your vimrc file.
" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
