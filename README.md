## Installation

```
git clone --recursive git@gitlab.com:morph027/vim8.git ~/.vim
git -C ~/.vim config submodule.pack/git-plugins/start/vim-jsonnet.ignore all
ln -s ~/.vim/.vimrc ~/.vimrc
ln -s ~/.vim/.editorconfig ~/.editorconfig
python3 -m virtualenv ~/.venv/
~/.venv/bin/pip install "python-lsp-server[all]" python-lsp-ruff ruff
brew install yaml-language-server bash-language-server yamllint fzf ripgrep jsonnet
```

## Run pylsp as service

### Linux

```
mkdir -p ~/.config/systemd/user
cat > ~/.config/systemd/user/pylsp.service <<EOF
[Unit]
Description=Python LSP Server - https://github.com/python-lsp/python-lsp-server

[Service]
ExecStart=%h/.venv/bin/pylsp --tcp
Restart=on-failure

[Install]
WantedBy=default.target
EOF
systemctl --user enable --now pylsp.service
```

### MacOS

```
mkdir -p ~/Library/LaunchAgents
cat > ~/Library/LaunchAgents/net.pylsp.server.plist <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>net.pylsp.server</string>
    <key>ProgramArguments</key>
    <array>
        <string>$HOME/.venv/bin/pylsp</string>
        <string>--tcp</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>LaunchOnlyOnce</key>
    <true/>
    <key>KeepAlive</key>
    <true/>
</dict>
</plist>
EOF
launchctl load Library/LaunchAgents/net.pylsp.server.plist
launchctl enable user/$(id -u $USER)/net.pylsp.server
launchctl start net.pylsp.server
```

## Hacks

### Search and replace in multiple files

* https://www.mattlayman.com/blog/2019/supercharging-vim-blazing-fast-search/

> Knowing this, what does a global find and replace look like with quickfix-reflector on? Let’s say you have a project, have fallen out of love with Python, and want to express your undying devotion to Ruby. 1 To change all occurrences of Python to Ruby, you can:
>
> * `:Rg` Python - Search for every mention of Python.
> * `VG` - Visually select all the lines in the quickfix list.
> * `:s/Python/Ruby/g` - Substitute Python for Ruby globally.
> * `:w` - Write the quickfix list to reflect those changes out to each individual file.

